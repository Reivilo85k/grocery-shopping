Grocery Shopping


Create class Product, it should contain at least two fields – name and price.
Create an empty array of Products – it’s size should be at least 5.
Populate the array with some products - this array represents the menu for the user.
Simulate the process of doing shopping:

ask for a product,
add it to the cart (array),
change index,
if index will be greater than 5 – finish shopping,
pay for the products.
