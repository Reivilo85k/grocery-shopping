import java.util.Scanner;

public class Product {
    private String name;
    private double price;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public static Product[] buy(Product[] cart) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 1; i++) {
            System.out.println("Available products : \n 1-Flour \n 2-Milk \n 3-Pasta \n 4-Pork \n 5-Tomatoes \n");
            cart = choseProduct(cart);
            System.out.println("\nDo you want to continue ? Y/N");
            String in = scanner.next();
            if (in.equals("Y") || in.equals("y") || in.equals("YES") || in.equals("yes") || in.equals("Yes")) {
                i--;
            }

        }
        return cart;
    }

    private static Product[] choseProduct(Product[] cart) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nplease input your product digit : ");
        String input = scanner.next();
        switch (input) {
            case "1":
                Flour flour = new Flour();
                cart = flour.addCart(cart, flour.getName(), flour.getPrice());
                System.out.println("Added flour " + flour.getPrice() + " to the cart");
                break;
            case "2":
                Milk milk = new Milk();
                cart = milk.addCart(cart, milk.getName(), milk.getPrice());
                System.out.println("Added 1 liter of milk (" + milk.getPrice() + ") to the cart");
                break;
            case "3":
                Pasta pasta = new Pasta();
                cart = pasta.addCart(cart, pasta.getName(), pasta.getPrice());
                System.out.println("Added 1 pack of pasta (" + pasta.getPrice() + ") to the cart");
                break;
            case "4":
                Pork pork = new Pork();
                cart = pork.addCart(cart, pork.getName(), pork.getPrice());
                System.out.println("Added 1Kg. pork (" + pork.getPrice() + ") to the cart");
                break;
            case "5":
                Tomatoes tomatoes = new Tomatoes();
                cart = tomatoes.addCart(cart, tomatoes.getName(), tomatoes.getPrice());
                System.out.println("Added 1Kg. tomatoes (" + tomatoes.getPrice() + ") to the cart");
                break;
            default:
                System.out.println("invalid input");
        }
        return cart;
    }

    public Product[] addCart(Product[] cart, String name, double price) {
        Product[] newCart = new Product[cart.length + 1];
        Product temp = new Product();
        temp.setName(name);
        temp.setPrice(price);
        int index = newCart.length - 1;

        for (int i = 0; i < cart.length; ) {
            newCart[i] = cart[i];
            i++;
        }
        newCart[index] = temp;
        return newCart;
    }
}