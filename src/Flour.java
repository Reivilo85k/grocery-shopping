public class Flour extends Product{
    private String name = "Flour";
    private double price = 3.2;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
