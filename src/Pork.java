public class Pork extends Product{
    private String name = "Pork";
    private double price = 3.10;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
