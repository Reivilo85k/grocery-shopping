import java.util.Scanner;

//Grocery Shopping
//
//Create class Product, it should contain at least two fields – name and price.
//Create an empty array of Products – it’s size should be at least 5.
//Populate the array with some products - this array represents the menu for the user.
//Simulate the process of doing shopping:
//
//ask for a product,
//add it to the cart (array),
//change index,
//if index will be greater than 5 – finish shopping,
//pay for the products.

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Product[] cart = new Product[0];

        cart = Product.buy(cart);

        System.out.println("\n\nYour Cart : \n\n");

        double totalPrice = 0;
        for (int j = 0; j < cart.length; j++) {
            totalPrice += cart[j].getPrice();
            System.out.println(cart[j].getName() + "  " + cart[j].getPrice());
        }
        System.out.println("-------------------------------");
        System.out.printf("Total = %.2f\n\n\nTHANK YOU FOR YOUR PURCHASE !!!\n\n\n\n ", totalPrice);

    }
}