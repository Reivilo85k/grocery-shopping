public class Milk extends Product {
    private String name = "Milk";
    private double price = 2.99;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
