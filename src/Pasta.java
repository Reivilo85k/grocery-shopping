public class Pasta extends Product {
   private String name = "Pasta";
   private double price = 2.99;

   public String getName() {
      return name;
   }

   public double getPrice() {
      return price;
   }
}
