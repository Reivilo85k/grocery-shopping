public class Tomatoes extends Product {
    private String name = "Tomatoes";
    private double price = 1.20;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
